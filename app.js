
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const indexRouter = require('./routes/index');
const userRouter = require('./routes/user');
const cartRouter = require('./routes/cart')
const homeRouter = require('./routes/home')
const orderRouter = require('./routes/order')
const seckillRouter = require('./routes/seckill')
const daichunyuan = require('./routes/daichunyuan')
const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/daichunyuan/', daichunyuan);
app.use('/', indexRouter);
app.use('/user/', userRouter);
app.use('/cart/', cartRouter);
app.use('/home/', homeRouter);
app.use('/order/', orderRouter);
app.use('/activity/seckill/', seckillRouter);

module.exports = app;
