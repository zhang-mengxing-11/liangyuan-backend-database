const { v4 } = require('uuid');
const getId = parseInt(v4().replace(/-/g, ''), 16)

module.exports = {
	getId
}