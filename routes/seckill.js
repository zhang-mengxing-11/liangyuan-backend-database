const express = require('express');
const router = express.Router();
const { seckillTimeModel, seckillSkuVoListModel } = require('../model/seckill');

router.get('/findAllSeckillTimeList', async (req, res) => {
	const data = await seckillTimeModel.find();
	res.send({
		code: 200,
		msg: '获取成功',
		data
	})
})

router.get('/findSeckillSkuList/:timeName', async (req, res) => {
	const { timeName } = req.params;
	const data = await seckillSkuVoListModel.findOne({ timeName });
	res.send({
		code: 200,
		msg: '获取成功',
		data
	})
})

module.exports = router;
