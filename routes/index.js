const express = require('express');
const router = express.Router();
const {
  regionListModel
} = require('../model/pickUpLocation')
const {
  categoryListModel,
  skuProductListModel,
  skushoopingModel
} = require('../model/product')
const {
  leaderAddressModel
} = require('../model/pickUpLocation')

const {
  submitOrderParamsModel
} = require('../model/order')

/*
* 支付订单
*
* */
router.get('/payment/weixin/createJsapi/:orderNo', (req, res) => {
  res.send({
    code: 200,
    msg: '获取成功'
  })
});

router.get('/submitOrder', async(req, res) => {
  const  data=await submitOrderParamsModel.find()
  res.send({
    code: 200,
    msg: '获取成功',
    data:data
  })
});

/*
* 根据经纬度获取地址信息
*
* */
router.get('/sys/region/findAllList', async (req, res) => {
  const data = await regionListModel.find()
  res.send({
    code: 200,
    msg: '获取成功',
    data
  })
});

router.get('/search/leader/:page/:limit', async (req, res) => {
  const { limit, page, latitude, longitude,address } = req.query
  let skip = (page - 1) * limit
  const data = await leaderAddressModel.find({address:address}).skip(skip).limit(limit)
  res.send({
    code: 200,
    msg: '获取成功',
    data: {
      content: data
    }
  })
});

router.get('/search/sku/:name/:page/:limit', async (req, res) => {
  const keyname=req.params.name
  const keypage=req.params.page
  const keylimit=req.params.limit
  const skipCount = (keypage - 1) * keylimit;
  const data = await skuProductListModel.find({title: keyname}).skip(skipCount).limit(keylimit).exec();
  res.send({
    code: 200,
    msg: '获取成功',
    data:data
    // data: {
    //   content: data,
    // }
  })
})
router.get('/search/data', async (req, res) => {
  const data = await skuProductListModel.find()
  res.send({
    code: 200,
    msg: '获取成功',
    data:data
    // data: {
    //   content: data,
    // }
  })
})
router.get('/user/leader/auth/selectLeader/:leaderId', (req, res) => {
  res.send({
    code: 200,
    msg: '获取成功'
  })
});

module.exports = router;
