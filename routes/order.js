const express = require('express');
const router = express.Router();
const {
	orderItemListModel,
	orderInfoListModel,
	orderRecordListModel,
	submitOrderParamsModel
} = require('../model/order')
const { skuProductListModel } = require('../model/product')
const { leaderAddressModel } = require('../model/pickUpLocation')
const {
	cartInfoListModel,
	activityRuleModel,
	couponInfoListModel,
} = require('../model/cart')


// 确认订单信息
router.get('/auth/confirmOrder', async (req, res) => {
	const data = await orderInfoListModel.findOne()
	const leaderAddressVo = await leaderAddressModel.find()
	const cartInfoList = await cartInfoListModel.find()
	const activityRule = await activityRuleModel.find()
	const couponInfoList = await couponInfoListModel.find()
	const carInfoVoList = [
		{
			activityRule,
			cartInfoList
		}
	]
	res.send({
		code: 200,
		msg: '获取成功',
		data: {
			activityReduceAmount: 2,
			couponReduceAmount: 3,
			originalTotalAmount: 120,
			totalAmount: 115,
			orderNo: 6.497177143428513e+37,
			leaderAddressVo: leaderAddressVo[0],
			couponInfoList,
			carInfoVoList
		}
	})
});

// 提交订单信息
router.post('/auth/submitOrder', async (req, res) => {
	console.log(req.body)
	await submitOrderParamsModel.create(req.body)
	res.send({
		code: 200,
		msg: '提交成功'
	})
});

/*
* 获取指定订单信息
*  orderId -订单信息orderId
* */
router.get('/auth/getOrderInfoById/:orderId', (req, res) => {

	res.send({
		code: 200,
		msg: '获取成功',
		data: []
	})
});

/*
*  查询用户订单列表
*
* */
router.get('/auth/findUserOrderPage/:pagemit', async (req, res) => {
	const { limit, page, orderStatus } = req.query
	let skip = (page - 1) * limit
	const data = await orderRecordListModel.find({ orderNo: orderStatus }).skip(skip).limit(limit)
	const total = await orderRecordListModel.find({ orderNo: orderStatus }).count()
	res.send({
		code: 200,
		msg: '获取成功',
		data: {
			total: total,
			size: limit,
			current: page,
			pages: 1,
			records: data
		}
	})
});

module.exports = router;
