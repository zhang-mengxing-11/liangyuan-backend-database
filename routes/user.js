const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const secretKey = 'role_id'
const { userModel } = require('../model/user')

router.get('/weixin/wxLogin/:code', function (req, res, next) {
  let code = req.params.code
  let token = jwt.sign({ code }, secretKey, { expiresIn: 1000 * 3600 })
  res.send({
    code: 201,
    msg: '登录成功',
    token
  })
});

router.post('/weixin/auth/updateUser', async (req, res) => {
  const data = await userModel.create(req.body)
  res.send({
    code: 200,
    msg: "更新数据成功",
    data
  })
})

module.exports = router;