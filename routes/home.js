const express = require('express');
const router = express.Router();
const {seckillTimeModel,seckillSkuVoListModel} = require('../model/seckill')
const {leaderAddressModel} = require('../model/pickUpLocation')
const {skuProductListModel,categoryListModel ,activityRuleListModel,imagesListModel,skuAttrValueListModel} = require('../model/product')
const {couponInfoListModel} = require('../model/cart')

// 获取首页信息
router.get('/index', async (req, res) => {
	const couponInfoList=await couponInfoListModel.find()
	const seckillTime = await seckillTimeModel.find()
	const leaderAddressVo = await leaderAddressModel.find()
	const newPersonSkuInfoList = await skuProductListModel.find()
	const categoryList = await categoryListModel.find()
	const hotSkuList = await skuProductListModel.find()
	const seckillSkuVoList = await seckillSkuVoListModel.find()
	res.send({
		code: 200,
		msg: '首页',
		data: {
			seckillTime: seckillTime[0],
			leaderAddressVo: leaderAddressVo[0],
			newPersonSkuInfoList,
			categoryList,
			hotSkuList,
			seckillSkuVoList,
			couponInfoList
		}
	})
});

router.get('/category', async (req, res) => {
	const data = await categoryListModel.find()
	res.send({
		code: 200,
		msg: '获取成功',
		data
	})
});

router.get('/item/:skuId', async (req, res) => {
	const { skuId } = req.params
	const skuInfoVo = await skuProductListModel.findOne({ id: skuId })
	const activityRuleList = await activityRuleListModel.find()
	const seckillSkuVo = await seckillSkuVoListModel.findOne({skuId})
	const couponInfoList = await couponInfoListModel.find()
	const skuPosterList = await imagesListModel.find()
	const skuAttrValueList = await  skuAttrValueListModel.find()
	res.send({
		code: 200,
		msg: '获取成功',
		data: {
			skuInfoVo: { ...skuInfoVo, skuPosterList, skuAttrValueList },
			activityRuleList,
			seckillSkuVo,
			couponInfoList
		}
	})
});

module.exports = router;
