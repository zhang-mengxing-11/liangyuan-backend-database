const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const secretKey = 'role_id'
var {usersModel,cityModel,addressModel}=require('../model/daichunyuan')

// 用户登录
router.post('/userLogin', async(req, res)=>{
    const body=req.body
    const code=body.code
    const token = jwt.sign({ code }, secretKey, { expiresIn: '1h' });
    try {
        const user = await usersModel.create({
          userName: body.data,
          userUrl: body.userurl,
          userAuth: body.userAuth,
          code:code
        });
        const userStatus = user.userStatus;
        // 判断 userStatus 是否为 false
        if (userStatus === false) {
          res.send({ code: 200, msg: '登录成功', token, userStatus: false });
        } else {
          res.send({ code: 200, msg: '登录成功', token, userStatus: true });
        }
      } catch (error) {
        console.error(error);
        res.status(500).send({ code: 500, msg: '登录失败' });
      }
});

module.exports = router;