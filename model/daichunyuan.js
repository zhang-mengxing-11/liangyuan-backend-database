const mongoose = require("./db");

// 用户表（昵称、头像、协议、token）
const usersSchema = new mongoose.Schema({
	userName: String,
	userUrl: String,
    userAuth:Boolean,
    userStatus:{type:Boolean,default:false}
},{versionKey:false})

// 城市表（城市名称）
const citySchema = new mongoose.Schema({
    cityName:String
},{versionKey:false})

// 地址表（用户ID、城市ID、店铺名称、距离、状态、图像）
const addressSchema = new mongoose.Schema({
    userid:String,
    cityid:String,
    title:String,
    distance:String,
    image:String,
    status:Boolean,
},{versionKey:false})


const usersModel = mongoose.model("users", usersSchema, "users");
const cityModel = mongoose.model("city", citySchema, "city");
const addressModel = mongoose.model("address", addressSchema, "address");

// 添加城市数据
// cityModel.create({
//     cityName:'石家庄'
// })


// 添加地址数据
// addressModel.create({
//     userid:'658a6c54162f788fce5442e2',
//     cityid:'658a6ba8fdfdc823c9ad365a',
//     title:'北京高碑店',
//     distance:'3.5',
//     image:'https://i02piccdn.sogoucdn.com/0e39045e37d50ee3',
//     status:false
// })

module.exports = { usersModel,cityModel,addressModel };


