const mongoose = require("./db");
const { getId } = require("../utils/getMethods");

const userSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true,
        unique: true,
        default: () => getId,
    },
    userType: {
        type: String,
    },
    photoUrl: {
        type: String,
        required: true,
    },
    nickName: {
        type: String,
        required: true,
    },
    sex: {
        type: Number,
        default: 0,
    },
    token: String,
});

const userModel = mongoose.model("user", userSchema, "user");

module.exports = { userModel };
