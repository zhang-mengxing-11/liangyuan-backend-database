const mongoose = require('./db');
const { getId } = require('../utils/getMethods')

const activityRuleSchema = new mongoose.Schema({
	id: {
		type: Number,
		default: () => getId
	},
	ruleDesc: {
		type: String,
		required: true
	}
})
const categoryListSchema = new mongoose.Schema({
	id: {
		type: Number,
		unique: true,
		required: true,
		default: () => getId
	},
	name: {
		type: String,
		required: true
	},
	imgUrl: {
		type: String,
		required: true
	},
	parentId: {
		type: Number,
		required: true,
	},
	status: {
		type: Number,
		required: true
	},
})
const imagesListSchema = new mongoose.Schema({
	id: {
		type: Number,
		default: () => getId
	},
	skuId: {
		type: Number
	},
	imgName: {
		type: String
	},
	imgUrl: {
		type: String
	}
})
const skuAttrValueListSchema = new mongoose.Schema({
	id: {
		type: Number,
		default: () => getId
	},
	skuId: {
		type: Number
	},
	attrId: {
		type: Number
	},
	attrName: {
		type: String
	},
	attrValue: {
		type: String
	}
})
const skuProductListSchema = new mongoose.Schema({
	id: {
		type: Number,
		unique: true,
		required: true,
		default: () => getId

	},
	keyword: {
		type: String,
		required: true
	},
	skuType: {
		type: Number,
		required: true,
		default: 1
	},
	isNewPerson: {
		type: Number,
		default: 0
	},
	categoryId: {
		type: Number,
		unique: true,
		required: true,
		default: () => getId

	},
	categoryName: {
		type: String,
	},
	imgUrl: {
		type: String,
		required: true
	},
	title: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	perLimit: {
		type: Number,
		required: true
	},
	sale: {
		type: Number,
		required: true
	},
	ruleList: Array,
	marketPrice: Number,
	skuName: String
})


const skushoopingSchema = new mongoose.Schema({
	id: {
		type: Number,

	},
	keyword: {
		type: String,
		required: true
	},
	skuType: {
		type: Number,
		required: true,
		default: 1
	},
	isNewPerson: {
		type: Number,
		default: 0
	},
	categoryId: {
		type: Number,

	},
	categoryName: {
		type: String,
	},
	imgUrl: {
		type: String,
		required: true
	},
	title: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	perLimit: {
		type: Number,
		required: true
	},
	sale: {
		type: Number,
		required: true
	},
	ruleList: Array,
	marketPrice: Number,
	skuName: String
})



const activityRuleListModel = mongoose.model('activityRuleList', activityRuleSchema, 'activityRuleList')
const skuProductListModel = mongoose.model('skuProductList', skuProductListSchema, 'skuProductList')
const skushoopingModel = mongoose.model('skushooping', skushoopingSchema, 'skushooping')
const categoryListModel = mongoose.model('categoryList', categoryListSchema, 'categoryList')
const imagesListModel = mongoose.model('imagesList',imagesListSchema,'imagesList')
const skuAttrValueListModel = mongoose.model('skuAttrValueList',skuAttrValueListSchema, 'skuAttrValueList')
module.exports = {
	activityRuleListModel,
	categoryListModel,
	skuProductListModel,
	imagesListModel,
	skuAttrValueListModel,
	skushoopingModel
}
// categoryListModel.create({
//     "imgUrl": "http://tmp/DtVQla2QSuq762b7930d6ec27fa88eed70e0776f67e7.jpeg",
//     "name": "图书",
//     "parentId": "11",
//     "status": "1"
// })
// skuProductListModel.create({
//     categoryName: 'categor2',
//     imgUrl: 'https://img0.baidu.com/it/u=1550899892,4205815413&fm=253&fmt=auto&app=138&f=JPEG?w=525&h=350',
//     isNewPerson: 1,
//     keyword: '蔬菜',
//     marketPrice: 1.9,
//     perLimit: 0,
//     price: 1.3,
//     ruleList: "[[满十元减两块]]",
//     sale: 0,
//     skuName: '生菜',
//     skuType: 0,
//     stock: '123',
//     title: '蔬菜'
// })