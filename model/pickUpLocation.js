const mongoose = require('./db');
const { getId } = require ( '../utils/getMethods' )

const regionListSchema = new mongoose.Schema({
	regionId: {type: Number,default: () => getId},
	regionName: {type: String,required: true}
})

const leaderAddressContentSchema = new mongoose.Schema({
	id: {type: Number,default: () => getId},
	takeName: {type: String,required: true},
	location: {type: Object},
	storePath: {type: String,required: true},
	detailAddress: {type: String,required: true},
	distance: {type: Number,required: true}
})

const leaderAddressSchema = new mongoose.Schema({
	userId: {type: Number,default: () => getId},
	leaderId: {type: Number,default: () => getId},
	leaderName: String,
	leaderPhone: String,
	takeName: {type: String,required: true},
	detailAddress: {type: String,required: true},
	longitude: {type: Number},
	latitude: {type: Number},
	storePath: {type: String,required: true},
	address:String
})

const regionListModel = mongoose.model('regionList', regionListSchema, 'regionList')
// const leaderAddressContentModel = mongoose.model('leaderAddress', leaderAddressContentSchema, 'leaderAddress')
const leaderAddressModel = mongoose.model('leaderAddress', leaderAddressSchema, 'leaderAddress')

module.exports = {
	leaderAddressModel,
	// leaderAddressContentModel,
	regionListModel
}
// leaderAddressModel.create({
// 	"detailAddress":"天鹅路旺旺超市",
// 	"latitude":"39.9729383416216",
// 	"leaderId":"303379639502775174585999694337601961984",
// 	"leaderName":"周小强",
// 	"leaderPhone":"13466789655",
// 	"longitude":"116.066284134167",
// 	"storePath":"https://img1.baidu.com/it/u=3784976360,1302185123&fm=253&fmt=auto&app=…",
// 	"takeName":"旺旺超市",
// 	"userId":"207468164680513141097288317998114799616",
// 	"distance":"500米",
// 	"address":"北京市",
// 	"id":222
// 	})