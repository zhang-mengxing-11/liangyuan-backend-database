const mongoose = require('./db');
const { getId } = require('../utils/getMethods')

const orderInfoListSchema = new mongoose.Schema({
	orderNo: {
		type: Number,
		required: true,
		unique: true,
		default: () => getId
	},
	activityReduceAmount: {
		type: Number,
		required: true
	},
	couponReduceAmount: {
		type: Number,
		required: true
	},
	originalTotalAmount: {
		type: Number,
		required: true
	},
	totalAmount: {
		type: Number,
		required: true
	}
})
// 订单列表
const orderItemListSchema = new mongoose.Schema({
	id: {
		type: Number,
		required: true,
		unique: true,
		default: () => getId
	},
	skuType: {
		type: String,
		required: true
	},
	skuId: {
		type: Number,
		required: true,
		unique: true,
		default: () => getId
	},
	skuName: {
		type: String,
		required: true
	},
	imgUrl: {
		type: String,
		required: true
	},
	skuPrice: {
		type: Number,
		required: true
	},
	skuNum: {
		type:  Number,
		required: true
	},
	splitActivityAmount: {
		type: Number,
		required: true
	},
	splitCouponAmount: {
		type: Number,
		required: true
	},
	splitTotalAmount: {
		type: Number,
		required: true
	}
})
const orderRecordListSchema = new mongoose.Schema({
	id: {
		type: Number,
		required: true,
		unique: true,
		default: () => getId
	},
	createTime: {
		type: String,
	},
	orderNo: {
		type: String
	},
	couponId: {
		type: Number
	},
	totalAmount: {
		type: Number
	},
	activityAmount: {
		type: Number
	},
	couponAmount: {
		type: Number
	},
	originalTotalAmount: {
		type: Number
	},
	paymentTime: {
		type: String
	},
	orderItemList: Array
})
const confirmOrderInfoSchema = new mongoose.Schema({
	id: {
		type: Number,
		required: true,
		unique: true,
		default: () => getId
	},
	createTime: {
		type:  String,
	},
	orderNo: {
		type:  String
	},
	couponId: {
		type:  String
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	activityAmount: {
		type: Number,
		default: 0
	},
	couponAmount: {
		type: Number,
		default: 0
	},
	originalTotalAmount: {
		type: Number,
		default: 0
	},
	leaderId: {
		type: Number
	},
	leaderName: {
		type: String
	},
	leaderPhone: {
		type: Number
	},
	takeName: {
		type: String
	},
	receiverName: {
		type: String
	},
	receiverPhone: {
		type: Number
	},
	receiverAddress: {
		type: String
	},
	orderItemList: Array
})
const confirmPaymentParamsSchema = new mongoose.Schema({
	timeStamp: {
		type: String,
		default: Date.now()
	},
	package: {
		type: String
	},
	paySign: {
		type: String
	},
	signType: {
		type: String
	},
	nonceStr: {
		type: String,
		default: getId
	}
})
const submitOrderParamsSchema = new mongoose.Schema({
	couponId: Number,
	leaderId: Number,
	orderNo: Number,
	receiverName: String,
	receiverPhone: Number
})

const orderInfoListModel = mongoose.model('orderInfoList', orderInfoListSchema, 'orderInfoList')
const orderItemListModel = mongoose.model('orderItemList', orderItemListSchema, 'orderItemList')
const orderRecordListModel = mongoose.model('orderRecordList', orderRecordListSchema, 'orderRecordList')
const submitOrderParamsModel = mongoose.model('submitOrderParams', submitOrderParamsSchema, 'submitOrderParams')

module.exports = {
	orderItemListModel,
	orderInfoListModel,
	orderRecordListModel,
	submitOrderParamsModel
}

