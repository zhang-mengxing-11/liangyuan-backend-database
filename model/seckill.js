const mongoose = require('./db');
const { getId } = require('../utils/getMethods')

// 秒杀信息
const seckillTimeSchema = new mongoose.Schema({
	id: {
		type: Number,
		unique: true,
		required: true,
		default: () => getId
	},
	name: {
		type: String,
		required: true,
	},
	startTime: {
		type: String,
		required: true,
	},
	endTime: {
		type: String,
		required: true,
	},
	status: {
		type: Number,
		required: true,
	},
	timeStaus: {
		type: Number,
		required: true
	}
})

// 秒杀商品列表
const seckillSkuVoListSchema = new mongoose.Schema({
	seckillSkuId: {
		type: Number,
		unique: true,
		required: true,
		default: () => getId
	},
	skuId: {
		type: Number
	},
	skuName: {
		type: String,
		required: true,
	},
	imgUrl: {
		type: String,
		required: true,
	},
	seckillPrice: {
		type: Number,
		required: true,
	},
	seckillStock: {
		type: Number,
		required: true,
	},
	seckillLimit: {
		type: Number,
		required: true,
	},
	seckillSale: {
		type: Number,
		required: true,
	},
	timeName: {
		type: String,
		required: true,
	},
	startTime: {
		type: String,
		required: true,
	},
	endTime: {
		type: String,
		required: true,
	},
})

// 秒杀时间段列表
const seckillTimeBucketItemSchema = new mongoose.Schema({
	id: {
		type: Number,
		unique: true,
		required: true,
		default: () => getId
	},
	name: {
		type: String,
		required: true,
	},
	startTime: {
		type: String,
		required: true,
	},
	endTime: {
		type: String,
		required: true,
	},
	timeStaus: {
		type: String,
		required: true
	}
})
//
const seckillTimeModel = mongoose.model('SeckillTime', seckillTimeSchema, 'seckillTime')
const seckillSkuVoListModel = mongoose.model('seckillSkuVoList', seckillSkuVoListSchema, 'seckillSkuVoList')
const seckillTimeBucketItemModel = mongoose.model('seckillTimeBucketItem', seckillTimeBucketItemSchema, 'seckillTimeBucketItem')



module.exports = {
	seckillTimeModel,
	seckillSkuVoListModel,
	seckillTimeBucketItemModel
}