const mongoose = require('./db');
const { getId } = require('../utils/getMethods')

// 购物车列表
const cartListSchema = new mongoose.Schema({
	skuId: {
		type: Number,
		required: true,
		ref:'cartInfoList'
	},
	skuNum: {
		type: Number,
		required: true,
		default: 0
	}
})
// 商品列表
const cartInfoListSchema = new mongoose.Schema({
	skuId: {
		type: Number,
		required: true,
		unique: true,
		default: () => getId
	},
	skuType: {
		type: Number,
	},
	isNewPerson: {
		type: Number,
	},
	skuName: {
		type: String,
	},
	cartPrice: {
		type: Number,
	},
	skuNum: {
		type: Number,
	},
	perLimit: {
		type: Number,
	},
	imgUrl: {
		type: String,
	},
	isChecked: {
		type: Number,
	},
	activityRule: Number
})

// 优惠券列表
const couponInfoListSchema = new mongoose.Schema({
	id: {
		type: Number,
		required: true,
		unique: true,
		default: () => getId
	},
	couponType: {
		type: String,
		required: true,
	},
	amount: {
		type: Number,
		required: true,
	},
	startTime: {
		type: String,
		required: true,
	},
	endTime: {
		type: String,
		required: true,
	},
	rangeType: {
		type: String,
		required: true,
	},
	rangeDesc: {
		type: String,
		required: true,
	},
	perLimit: {
		type: String,
		required: true,
	},
	expireTime: {
		type: String,
		required: true,
	},
	couponStatus: {
		type: Number,
		required: true,
	},
	isSelect: {
		type: Number,
		required: true,
	},
	isOptimal: {
		type: String,
		required: true,
	},
})
// 活动规则列表
const activityRuleSchema= new  mongoose.Schema({
	id: {
		type: Number,
		required: true,
		unique: true,
		default: () => getId
	},
	ruleDesc: {
		type: String,
		required: true,
	},
})
// 活动列表
const activityCartListSchema  = new mongoose.Schema({
	activityReduceAmount: {
		type:  Number,
		required: true,
	},
	couponReduceAmount: {
		type: Number,
		required: true,
	},
	originalTotalAmount: {
		type: Number,
		required: true,
	},
	totalAmount: {
		type: Number,
		required: true
	}
})

const cartListModel = mongoose.model('cartList', cartListSchema, 'cartList')
const cartInfoListModel = mongoose.model('cartInfoList', cartInfoListSchema, 'cartInfoList');
const couponInfoListModel = mongoose.model('couponInfoList', couponInfoListSchema, 'couponInfoList');
const activityRuleModel = mongoose.model('activityRule', activityRuleSchema, 'activityRule');
const activityCartListModel = mongoose.model('activityCartList', activityCartListSchema, 'activityCartList')


// couponInfoListModel.create({
// 	amount:"5",
// 	couponStatus:"1",
// 	couponType:"新品",
// 	endTime:"2024-12-17",
// 	expireTime:"2020-05-01 00:00:00",
// 	isOptimal:"1",
// 	isSelect:"1",
// 	perLimit:"99",
// 	rangeDesc:"购买任意商品，即可使用",
// 	rangeType:"全部商品",
// 	startTime:"2023-12-27"
// })
module.exports = {
	cartListModel,
	cartInfoListModel,
	couponInfoListModel,
	activityRuleModel,
	activityCartListModel
}
